## Requirements

| Name | Version |
|------|---------|
| terraform | 0.12.21 |
| aws | 2.69.0 |
| helm | 1.2.2 |
| kubernetes | 1.13.3 |

## Providers

| Name | Version |
|------|---------|
| aws | 2.69.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| availability\_zones | (Required) - The AWS avaialbility zones (e.g. ap-southeast-2a/b/c). Autoloaded from region.tfvars. | `list(string)` | n/a | yes |
| aws\_access\_key\_id | (Required) - The AWS STS reuiqred access key id | `string` | n/a | yes |
| aws\_account\_alias | (Required) - The AWS account alias of the provider being deployed to. Autoloaded from account.tfvars | `string` | n/a | yes |
| aws\_account\_id | (Required) - The AWS account id of the provider being deployed to (e.g. 12345678). Autoloaded from account.tfvars | `string` | n/a | yes |
| aws\_region | (Required) - The AWS region (e.g. ap-southeast-2). Autoloaded from region.tfvars. | `string` | n/a | yes |
| aws\_secret\_access\_key | (Required) - The AWS STS reuiqred secret access key | `string` | n/a | yes |
| eks\_cluster\_certificate\_authority\_data | The Kubernetes cluster certificate authority data | `string` | n/a | yes |
| eks\_cluster\_endpoint | The endpoint for the Kubernetes API server | `string` | n/a | yes |
| eks\_cluster\_id | The name of the EKS cluster | `string` | n/a | yes |
| kubeconfig\_context | (Required) The context to use from the `kubeconfig` file | `string` | n/a | yes |
| remote\_state\_bucket | (Required) - The AWS s3 bucket being used to call a remote terraform state configuration. Autoloaded from root terragrunt.hcl | `string` | n/a | yes |
| remote\_state\_bucket\_region | (Required) - The AWS region of the s3 bucket being used to call the remote terraform state configuration (e.g. ap-southeast-2). Autoloaded from region.tfvars. | `string` | n/a | yes |
| workers\_role\_name | Name of the worker nodes IAM role | `string` | n/a | yes |
| attributes | (Optional) - Additional attributes (e.g. `1`) | `list(string)` | `[]` | no |
| aws\_assume\_role\_arn | (Optional) - ARN of the IAM role when optionally connecting to AWS via assumed role. Autoloaded from account.tfvars. | `string` | `""` | no |
| aws\_assume\_role\_external\_id | (Optional) - The external ID to use when making the AssumeRole call. | `string` | `""` | no |
| aws\_assume\_role\_session\_name | (Optional) - The session name to use when making the AssumeRole call. | `string` | `""` | no |
| aws\_iam\_serial\_number | (Optional) - MFA device ARN of an IAM user. | `string` | `""` | no |
| environment | (Optional) - Environment, e.g. 'dev', 'qa', 'staging', 'prod' | `string` | `""` | no |
| externaldns | All ExternalDNS module related variables. | <pre>object({<br>    enabled       = bool<br>    namespace     = string<br>    chart_version = string<br>    set = list(object({<br>      name  = string<br>      value = string<br>    }))<br>    set_string = list(object({<br>      name  = string<br>      value = string<br>    }))<br>    set_sensitive = list(object({<br>      name  = string<br>      value = string<br>    }))<br>  })</pre> | <pre>{<br>  "chart_version": "2.16.1",<br>  "enabled": false,<br>  "namespace": "kube-system",<br>  "set": [],<br>  "set_sensitive": [],<br>  "set_string": []<br>}</pre> | no |
| name | (Optional) - Solution name, e.g. 'vault', 'consul', 'keycloak', 'k8s', or 'baseline' | `string` | `""` | no |
| namespace | (Optional) - Namespace, which could be your abbreviated product team, e.g. 'rci', 'mi', 'hp', or 'core' | `string` | `""` | no |
| nginxingress | All Nginx Ingress module related variables. | <pre>object({<br>    enabled       = bool<br>    namespace     = string<br>    chart_version = string<br>    values        = list(string)<br>    set = list(object({<br>      name  = string<br>      value = string<br>    }))<br>    set_string = list(object({<br>      name  = string<br>      value = string<br>    }))<br>    set_sensitive = list(object({<br>      name  = string<br>      value = string<br>    }))<br>  })</pre> | <pre>{<br>  "chart_version": "1.30.1",<br>  "enabled": false,<br>  "namespace": "kube-system",<br>  "set": [],<br>  "set_sensitive": [],<br>  "set_string": [],<br>  "values": []<br>}</pre> | no |
| shared\_services\_assume\_role\_arn | ARN of the IAM role when optionally connecting to AWS via assumed role. Autoloaded from account.hcl. | `string` | `""` | no |
| shared\_services\_assume\_role\_external\_id | The external ID to use when making the AssumeRole call. | `string` | `""` | no |
| shared\_services\_assume\_role\_session\_name | The session name to use when making the AssumeRole call. | `string` | `""` | no |
| tiller\_service\_account | The name of the service account for tiller | `string` | `"helm-tiller"` | no |

## Outputs

No output.

