# -----------------------------------------------------------------------------
# Data: AWS REGION - AWS Region To Deploy Into (Will Use the Assumed Role Defaults)
# -----------------------------------------------------------------------------

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

# -----------------------------------------------------------------------------
# Data: EKS authentication - for use with Helm and Kubernetes providers
# -----------------------------------------------------------------------------

data "aws_eks_cluster" "default" {
  name = var.eks_cluster_id
}

data "aws_eks_cluster_auth" "default" {
  name = var.eks_cluster_id
}

# -----------------------------------------------------------------------------
# Provider: Configuration - Autoloaded From Terragrunt
# -----------------------------------------------------------------------------

provider "aws" {
  region = var.aws_region
  assume_role {
    role_arn     = var.aws_assume_role_arn
    session_name = var.aws_assume_role_session_name
    external_id  = var.aws_assume_role_external_id
  }
}

provider "helm" {
  version         = "1.2.2"
  kubernetes {
    host                   = var.eks_cluster_endpoint
    cluster_ca_certificate = base64decode(var.eks_cluster_certificate_authority_data)
    token                  = data.aws_eks_cluster_auth.default.token
    config_context         = var.kubeconfig_context
  }
}

provider "kubernetes" {
  host                   = var.eks_cluster_endpoint
  cluster_ca_certificate = base64decode(var.eks_cluster_certificate_authority_data)
  token                  = data.aws_eks_cluster_auth.default.token
  load_config_file       = false
}

# -----------------------------------------------------------------------------
# Resource Group: Project AWS Inspector Resource Group
# -----------------------------------------------------------------------------

resource "aws_resourcegroups_group" "default" {
  name = module.label.id
  resource_query {
    query = <<JSON
      {
        "ResourceTypeFilters": [
          "AWS::AllSupported"
        ],
        "TagFilters": [
          {
            "Key": "Environment",
            "Values": ["${var.environment}"]
          },
                    {
            "Key": "Name",
            "Values": ["${var.name}"]
          }
        ]
      }
      JSON
  }
}

# -----------------------------------------------------------------------------
# Module: ExternalDNS
# -----------------------------------------------------------------------------
data "aws_iam_policy_document" "edns_policy" {
  count = var.externaldns.enabled ? 1 : 0
  statement {
    effect    = "Allow"
    resources = ["arn:aws:route53:::hostedzone/*"]
    actions   = ["route53:ChangeResourceRecordSets"]
  }

  statement {
    effect    = "Allow"
    resources = ["*"]

    actions = [
      "route53:ListHostedZones",
      "route53:ListResourceRecordSets",
    ]
  }
}
resource "aws_iam_policy" "external_dns" {
  count  = var.externaldns.enabled ? 1 : 0
  name   = format("%s%s%s", module.label.id, module.label.delimiter, "external_dns")
  path   = "/"
  policy = data.aws_iam_policy_document.edns_policy.0.json
}
resource "aws_iam_role_policy_attachment" "edns_workers_role" {
  count      = var.externaldns.enabled ? 1 : 0
  role       = var.workers_role_name
  policy_arn = aws_iam_policy.external_dns.0.arn
}
module "external_dns" {
  source                = "git::ssh://git@gitlab.com/eggplantsoftware/platform/terraform/mod-helm.git?ref=helm3"
  enabled               = var.externaldns.enabled
  verify                = false
  release_name          = "external-dns"
  chart_repository_name = "bitnami"
  namespace             = var.externaldns.namespace
  chart_repository_url  = "https://charts.bitnami.com/bitnami"
  chart                 = "external-dns"
  chart_version         = var.externaldns.chart_version
  set                   = var.externaldns.set
  set_string            = var.externaldns.set_string
  set_sensitive         = var.externaldns.set_sensitive
}

# -----------------------------------------------------------------------------
# Module: NGINX Ingress
# -----------------------------------------------------------------------------
module "nginx_ingress" {
  source                = "git::ssh://git@gitlab.com/eggplantsoftware/platform/terraform/mod-helm.git?ref=helm3"
  enabled               = var.nginxingress.enabled
  verify                = false
  release_name          = "ingress-nginx"
  chart_repository_name = "ingress-nginx"
  namespace             = var.nginxingress.namespace
  chart_repository_url  = "https://kubernetes.github.io/ingress-nginx"
  chart                 = "ingress-nginx"
  chart_version         = var.nginxingress.chart_version
  values                = var.nginxingress.values
  set                   = var.nginxingress.set
  set_string            = var.nginxingress.set_string
  set_sensitive         = var.nginxingress.set_sensitive
}

# -----------------------------------------------------------------------------
# Module: Rancher
# -----------------------------------------------------------------------------

module "rancher" {
  source		= "git::ssh://git@gitlab.com/eggplantsoftware/platform/terraform/mod-helm.git?ref=helm3"
  verify                = false
  chart_repository_name = "rancher-stable"
  chart_repository_url  = "https://releases.rancher.com/server-charts/stable"
  release_name          = "rancher"
  chart                 = "rancher"
  chart_version         = "2.5.5"
  namespace             = "cattle-system"
  set                   = [
    {
      name  = "hostname"
      value = "rancher.dev.core.webperfdev.com"
    },
    {
      name  = "tls"
      value = "external"
    }
  ]
}