terraform {
  backend "s3" {}
  required_version = "0.12.21"
  required_providers {
    aws = "2.69.0"
    kubernetes = "1.13.3"
  }
}