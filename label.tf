# -----------------------------------------------------------------------------
# Module: TF-MOD-LABEL - https://gitlab.com/eggplantsoftware/platform/terraform/mod-label
# -----------------------------------------------------------------------------

module "label" {
  source             = "git::ssh://git@gitlab.com/eggplantsoftware/platform/terraform/mod-label.git?ref=develop"
  namespace          = var.namespace
  environment        = var.environment
  name               = var.name
  attributes         = var.attributes
  delimiter          = "-"
  additional_tag_map = {} /* Additional attributes (e.g. 1) */
  label_order        = ["namespace", "environment", "name", "attributes"]
}