# -----------------------------------------------------------------------------
# Variables: Common AWS Provider - Autoloaded from Terragrunt
# -----------------------------------------------------------------------------

variable "aws_region" {
  description = "(Required) - The AWS region (e.g. ap-southeast-2). Autoloaded from region.tfvars."
  type        = string
}

variable "aws_assume_role_arn" {
  description = "(Optional) - ARN of the IAM role when optionally connecting to AWS via assumed role. Autoloaded from account.tfvars."
  type        = string
  default     = ""
}

variable "aws_assume_role_session_name" {
  description = "(Optional) - The session name to use when making the AssumeRole call."
  type        = string
  default     = ""
}

variable "aws_assume_role_external_id" {
  description = "(Optional) - The external ID to use when making the AssumeRole call."
  type        = string
  default     = ""
}

# -----------------------------------------------------------------------------
# Variables: TF-MOD-EKS-CLUSTER - https://gitlab.com/eggplantsoftware/platform/terraform/mod-eks-cluster
# -----------------------------------------------------------------------------

variable "eks_cluster_id" {
  type        = string
  description = "The name of the EKS cluster"
}

variable "eks_cluster_endpoint" {
  type        = string
  description = "The endpoint for the Kubernetes API server"
}

variable "eks_cluster_certificate_authority_data" {
  type        = string
  description = "The Kubernetes cluster certificate authority data"
}

variable "kubeconfig_context" {
  type        = string
  description = "(Required) The context to use from the `kubeconfig` file"
}

variable "workers_role_name" {
  type        = string
  description = "Name of the worker nodes IAM role"
}

# -----------------------------------------------------------------------------
# Variables: TF-MOD-LABEL - https://gitlab.com/eggplantsoftware/platform/terraform/mod-label - Autoloaded from Terragrunt
# -----------------------------------------------------------------------------

variable "namespace" {
  type        = string
  default     = ""
  description = "(Optional) - Namespace, which could be your abbreviated product team, e.g. 'rci', 'mi', 'hp', or 'core'"
}

variable "environment" {
  type        = string
  default     = ""
  description = "(Optional) - Environment, e.g. 'dev', 'qa', 'staging', 'prod'"
}

variable "name" {
  type        = string
  default     = ""
  description = "(Optional) - Solution name, e.g. 'vault', 'consul', 'keycloak', 'k8s', or 'baseline'"
}

variable "attributes" {
  type        = list(string)
  default     = []
  description = "(Optional) - Additional attributes (e.g. `1`)"
}

# -----------------------------------------------------------------------------
# Variables: ExternalDNS
# -----------------------------------------------------------------------------
variable "externaldns" {
  type = object({
    enabled       = bool
    namespace     = string
    chart_version = string
    set = list(object({
      name  = string
      value = string
    }))
    set_string = list(object({
      name  = string
      value = string
    }))
    set_sensitive = list(object({
      name  = string
      value = string
    }))
  })
  default = {
    enabled       = false
    namespace     = "kube-system"
    chart_version = "2.16.1"
    set           = []
    set_string    = []
    set_sensitive = []
  }
  description = "All ExternalDNS module related variables."
}

# -----------------------------------------------------------------------------
# Variables: Nginx Ingress
# -----------------------------------------------------------------------------
variable "nginxingress" {
  type = object({
    enabled       = bool
    namespace     = string
    chart_version = string
    values        = list(string)
    set = list(object({
      name  = string
      value = string
    }))
    set_string = list(object({
      name  = string
      value = string
    }))
    set_sensitive = list(object({
      name  = string
      value = string
    }))
  })
  default = {
    enabled       = false
    namespace     = "kube-system"
    chart_version = "1.30.1"
    values        = []
    set           = []
    set_string    = []
    set_sensitive = []
  }
  description = "All Nginx Ingress module related variables."
}